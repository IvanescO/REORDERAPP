import React, { Component } from "react";
import { connect } from "react-redux";
import { View, Text, StyleSheet,Platform } from "react-native";
import { Logout } from "../actions/userActions";
import StyleConst from "../styleConst";
import Icon from "react-native-vector-icons/FontAwesome";
import { NavigationActions } from "react-navigation";
import { changeRoute } from "../actions/routerAction";

function mapStateToProps(state) {
  return {};
}

class ToolBar extends Component {
  goToOrderList = () => {
    let { billing } = this.props;
    console.log(billing);
    if (billing) return;
    const navigateToOrder = NavigationActions.navigate({
      routeName: "Order"
    });
    this.props.changeRoute(navigateToOrder);
  };
  Logout = ()=> {
    
    let {billing} = this.props;
    console.log(billing);
    if(billing) return;

    console.log('here', this.props);
    
    this.props.Logout();
  }
  render() {
    const { toolbar, toolbarButton, toolbarTitle } = styles;
    const { title, Logout, showList } = this.props;
    console.log(Logout);
    return <View style={styles.toolbar}>
       {showList && <Text style={{ paddingLeft: 10 }} onPress={this.goToOrderList}>
            <Icon name="list" size={20} color="#fff" />
        </Text> }
        {!showList &&<Text style={styles.toolbarButton} />}
        <Text style={styles.toolbarTitle}>{title}</Text>
        <Text style={{ paddingRight: 15 }} onPress={this.Logout}>
          <Icon name="sign-out" size={20} color="#fff" />
        </Text>
      </View>;
  }
}
const styles = StyleSheet.create({
  toolbar: {
    backgroundColor: StyleConst.buttonColor,
    borderBottomColor: "white",
    borderBottomWidth: 1,
    height: Platform.OS === 'ios' ? 100 :  100 - 24,
    paddingTop: 20,
    paddingBottom: 10,
    alignItems: "center",
    // justifyContent:"center",
    flexDirection: "row"
  },
  toolbarButton: {
    width: 50,
    color: "#fff",
    textAlign: "center"
  },
  toolbarTitle: {
    color: "#fff",
    textAlign: "center",
    fontWeight: "bold",
    flex: 1,
    fontSize: 18
  }
});


export default connect(mapStateToProps, { Logout, changeRoute })(ToolBar);

