export function setOneSignalData(data) {
  return  dispatch => {
    dispatch({
      type: "SET_ONE_SIGNAL_DATA",
      payload: { pushToken: data.pushToken, userNotificationsId:data.userId }
    });
  };
}
