import UserService from '../api/userApi';
import OrderService from '../api/orderApi';
import { AsyncStorage } from 'react-native';
import { NavigationActions } from 'react-navigation';
export function userIsAuth(){
  return async(dispatch) => {
    let token = await AsyncStorage.getItem("token");
    if(!token){
      return dispatch({
            type: "LOGOUT",
            payload: { error: "" }
          });
    }
    try {
      const response = await UserService.getMe(token);
      const user = response.data;          
      dispatch({type:"SET_USER", payload:user})
    } catch(e) {
      dispatch({ type: "LOGOUT", payload: { error: "" } });
    }
  }

}
//let Obj =  require("node-libs-react-native");
//console.log(Obj);
export function updSettings(settings){
    return async (dispath) => {
        let response = await UserService.updSettings(settings);
        let user = response.data;
        AsyncStorage.setItem("settings", JSON.stringify(settings));
        dispath({ type: "UPD_USER", payload: user });
    };
}
export function userLogin(username, password, userNotificationsId) {
    return async dispatch => {
      try {
        const response = await UserService.Login(username, password, userNotificationsId);
        console.log(response);
        const { token } = response.data;
        await AsyncStorage.setItem("token", token);
        dispatch({ type: "LOGIN", payload: { token } });
        const userResponse = await UserService.getMe(token);
        dispatch({
          type: "SET_USER",
          payload: userResponse.data
        });
      } catch (e) {
        return dispatch({
          type: "LOGIN_ERROR",
          payload: e
        });
      }
    };
}
export function getOrders(){
    return async(dispatch) => {
        dispatch({
          type: "ORDERS_LOADING",
          payload: { ordersLoading: true }
        });
        try {
        let response = await OrderService.getMyOrder();
        console.log(response);
        const orders = response.data.orders;
        const total_debt = response.data.credit_account;
        dispatch({
          type: "SET_ORDERS",
          payload: { orders, ordersLoading: false, total_debt}
        });
      } catch (e){
        if(e.response.data.status === 401) {
          return dispatch({
           type: "LOGOUT",
           payload: {
             error: "Der Benutzername oder das Passwort ist ungültig",
             ordersLoading: false
           }
         });
        }
      }
    }
}
export function updOrder(orderId, status){
  return async(dispatch) => {
      try {
        let response = await OrderService.updOrder(orderId, status);
        response = await OrderService.getMyOrder();
        const orders = response.data.orders;
        const order = orders.find(order => {
            return order.id == orderId;
        });
        const total_debt = response.data.credit_account;
        dispatch({
          type: "SET_ORDERS",
          payload: { orders, total_debt }
        });
        if(status != "cancel")
        dispatch({type:"SELECT_ORDER", payload: order});
      } catch (e) {
        if (e.response.data.status === 401) {
          return dispatch({
            type: "LOGOUT",
            payload: {
              error: "Der Benutzername oder das Passwort ist ungültig",
              ordersLoading: false
            }
          });
        }
      }
  }
}
export function selectOrder(order){
    return async(dispath) => {
        dispath({type:"SELECT_ORDER",payload:order});
    }
}
export function Logout(error) {
         return async dispatch => {
          //  const res = await UserService.Logout();
           await AsyncStorage.clear();
           dispatch({
             type: "LOGOUT",
             payload: { error: error || '' }
           });
         };
       }
export function addTransaction(orderId, amount, status, logs){
    console.log(status)
    return async( dispatch ) => {
      try {
        let response = await OrderService.addTranscation(orderId, amount, status, logs);
      } catch (e){
        return dispatch({
          type: "LOGOUT",
          payload: {
            error: "Der Benutzername oder das Passwort ist ungültig"
          }
        });
      }
        if(status){
          try{
            const order = await OrderService.getOrder(orderId);
            dispatch({
                type: "SELECT_ORDER",
                payload: order.data
            });
          } catch(e){
            if (e.response.data.status === 401) {
              return dispatch({
                type: "LOGOUT",
                payload: {
                  error:
                    "Der Benutzername oder das Passwort ist ungültig",
                }
              });
            } else {
              const navigateToOrder = NavigationActions.navigate({
                routeName: "Order"
              });
              return dispatch(navigateToOrder);
            }
          }
        }
    }
}