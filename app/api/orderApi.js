import axios from "axios";
import { AsyncStorage } from "react-native";
import apiConfig from './apiConfig';
import axiosRetry from "axios-retry";
axios.interceptors.response.use(null, err => {
  var config = err.config;
  // If config does not exist or the retry option is not set, reject
  console.log(config.retry);
  if (!config || !config.retry) return Promise.reject(err);

  // Set the variable for keeping track of the retry count
  config.__retryCount = config.__retryCount || 0;

  // Check if we've maxed out the total number of retries
  if (config.__retryCount >= config.retry) {
    // Reject with the error
    return Promise.reject(err);
  }

  // Increase the retry count
  config.__retryCount += 1;

  // Create new promise to handle exponential backoff
  var backoff = new Promise(function(resolve) {
    setTimeout(function() {
      resolve();
    }, config.retryDelay || 1);
  });

  // Return the promise in which recalls axios to retry the request
  return backoff.then(function() {
    return axios(config);
  });

  return Promise.reject(error);
});
class OrderService {
  //url = "http://re-order.smissltd.com/rest/orders";
  async getMyOrder() {
    const token = await AsyncStorage.getItem("token");
    let body = {};
    const headers = { headers: { Authorization: "Bearer " + token } };
    const getMyOrder = apiConfig.apiUrl+"orders";
    return axios.post(getMyOrder, body, headers);
  }
  async getOrder(id){
    const token = await AsyncStorage.getItem("token");
    let body = new FormData();
    // body.append("token", token);
    body.append("id", id);
    const headers = { headers: { Authorization: "Bearer " + token } };
    const getOrder = apiConfig.apiUrl+"order";
    return axios.post(getOrder, body, headers);
  }
  async updOrder(orderId, status) {
    const token = await AsyncStorage.getItem("token");
    let body = new FormData();
    const headers = { headers: { Authorization: "Bearer " + token } };
    body.append("id", orderId);
    body.append("status", status);
    const setOrderStatus = apiConfig.apiUrl+"order-status";
    return axios.post(setOrderStatus, body);
  }
  async addTranscation(orderId, amount, status, logs){
    console.log(orderId,logs);
    const token = await AsyncStorage.getItem("token");
    let body = new FormData();
    // body.append("token", token);
    body.append("amount", amount);
    body.append("id", orderId);
    body.append("messages", logs);
    body.append("success", status);
    const headers = { headers: { Authorization: "Bearer " + token }, retry: 10, retryDelay: 2000 };
    const addTranscationUrl = apiConfig.apiUrl + "order-transaction";
    let req;
    // try {
      req = await axios.post(addTranscationUrl, body,headers);
    // } catch (e){
    //   console.log(e);
    // }
    return req;
  }
}

export default new OrderService();
