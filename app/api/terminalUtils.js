class terminalUtils {
  hex2bin(s) {
    var ret = [];
    var i = 0;
    var l;
    s += "";
    for (l = s.length; i < l; i += 2) {
      var c = parseInt(s.substr(i, 1), 16);
      var k = parseInt(s.substr(i + 1, 1), 16);
      if (isNaN(c) || isNaN(k)) return false;
      ret.push((c << 4) | k);
    }
    return String.fromCharCode.apply(String, ret);
  }
  bin2hex(s) {
    var i,
      l,
      o = "",
      n;

    s += "";

    for (i = 0, l = s.length; i < l; i++) {
      n = s.charCodeAt(i).toString(16);
      o += n.length < 2 ? "0" + n : n;
    }

    return o;
  }
  base_convert(number, frombase, tobase) {
    // eslint-disable-line camelcase
    return parseInt(number + "", frombase | 0).toString(tobase | 0);
  }
}

export default new terminalUtils();
