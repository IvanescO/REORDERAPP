import axios from 'axios';
import { AsyncStorage } from 'react-native';
import apiConfig from "./apiConfig";
class UserService {
  url = "http://172.16.3.22:8000/user/";
  Login(username, password, userNotificationsId) {
    let body = new FormData();
    body.append("username", username);
    body.append("password", password);
    body.append("device_id", userNotificationsId)
    const loginUrl = apiConfig.apiUrl+"login";
    return axios.post(loginUrl, body);
  }
  async Logout() {
    let body = new FormData();
    const token = await AsyncStorage.getItem("token");
    body.append("token", token);
    const logoutUrl = apiConfig.apiUrl+"logout";
    return axios.post(logoutUrl, body);
  }
  async getMe(token) {
    const headers = { headers: { Authorization: "Bearer " + token } }; //the token is a variable which holds the token
    const getMeUrl = apiConfig.apiUrl+"user";
    return axios.post(getMeUrl,{} ,headers);
  }
  async updSettings({ ip, port }) {
    const updSettingsUrl = this.url + "settings";
    const token = await AsyncStorage.getItem("token");
    return axios.put(
      updSettingsUrl,
      { ip, port },
      {
        headers: { authorization: "Bearer " + token }
      }
    );
  }
}

export default new UserService();