import { StackNavigator } from "react-navigation";
import LoginScreen from "../pages/login";
import OrderScreen from "../pages/order";
import SettingsScreen from "../pages/settings";
import OrderInfoScreen from "../pages/orderInfo";
const navigator = StackNavigator({
  Login: { screen: LoginScreen },
  Order: { screen: OrderScreen },
  Settings: { screen: SettingsScreen },
  orderInfo: { screen: OrderInfoScreen }
});

export default navigator;