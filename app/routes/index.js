import React, { Component } from "react";
import { BackHandler } from "react-native";
import { connect } from "react-redux";
import { addNavigationHelpers, NavigationActions } from "react-navigation";
import NavigationStack from "./navigationStack";
import { changeRoute } from "../actions/routerAction";
import {userIsAuth, getOrders} from '../actions/userActions';
import OneSignal from 'react-native-onesignal'; 
import { setOneSignalData } from "../actions/oneSignalAction";
import {createReduxBoundAddListener} from 'react-navigation-redux-helpers';
class AppNavigation extends Component {
  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
  }
  componentWillMount() {
    // OneSignal.addEventListener("received", this.onReceived);
    // OneSignal.addEventListener("opened", this.onOpened);
    // OneSignal.addEventListener("registered", this.onRegistered);
    // OneSignal.addEventListener("ids", this.onIds);
    this.props.userIsAuth();
  }
  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    // OneSignal.removeEventListener("received", this.onReceived);
    // OneSignal.removeEventListener("opened", this.onOpened);
    // OneSignal.removeEventListener("registered", this.onRegistered);
    // OneSignal.removeEventListener("ids", this.onIds);
  }
  onReceived(notification) {
    console.log("Notification received: ", notification);
    this.props.getOrders();
  }

  onOpened(openResult) {
    console.log("Message: ", openResult.notification.payload.body);
    console.log("Data: ", openResult.notification.payload.additionalData);
    console.log("isActive: ", openResult.notification.isAppInFocus);
    console.log("openResult: ", openResult);
  }

  onRegistered(notifData) {
    console.log(
      "Device had been registered for push notifications!",
      notifData
    );
  }

  onIds = (device) => {
    console.log("Device info: ", device);
    console.log(this.props);
    this.props.setOneSignalData(device);
  }
  onBackPress = () => {
    const { changeRoute, navigationState } = this.props;
    if (navigationState.stateForLoggedIn.index <= 1) {
      BackHandler.exitApp();
      return;
    }
    changeRoute(NavigationActions.back());
    return true;
  };

  render() {
    const { navigationState, changeRoute, isLoggedIn, user } = this.props;
    const state = isLoggedIn
      ? navigationState.stateForLoggedIn
      : navigationState.stateForLoggedOut;
      const addListener = createReduxBoundAddListener("root");
    return <NavigationStack navigation={addNavigationHelpers({
          changeRoute,
          dispatch: this.props.dispatch,
          state,
          addListener
        })} />;
  }
}
const mapStateToProps = state => {
  return { 
    nav: state.nav,
    isLoggedIn: state.userReducer.isLoggedIn,
     navigationState: state.navigationReducer };
};
const mapDispatchToProps = dispatch => {
  return { userIsAuth, changeRoute, getOrders, setOneSignalData, dispatch };
}
export default connect(mapStateToProps, mapDispatchToProps)(AppNavigation);
