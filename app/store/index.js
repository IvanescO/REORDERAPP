import { createStore, combineReducers, applyMiddleware } from "redux";
import {
  persistCombineReducers,
  persistStore,
  persistReducer
} from "redux-persist";
import storage from "redux-persist/es/storage";
import thunk from "redux-thunk";
import UserReducer from "../reducers/userReducer";
import NavigationReducer from "../reducers/navigaionReducer";
import OneSignalReducer from "../reducers/oneSignalReducer";
import {
  createReduxBoundAddListener,
  createReactNavigationReduxMiddleware,
} from 'react-navigation-redux-helpers';
// config to not persist the *counterString* of the CounterReducer's slice of the global state.
const config = {
  key: "root",
  storage,
  blacklist: ["loginError"]
};

const config1 = {
  key: "primary",
  storage
};
const config2 = {
  key: "oneSignal",
  storage
};

// Object of all the reducers for redux-persist
const reducer = {
  UserReducer,
  NavigationReducer
};


const userReducer = persistReducer(config, UserReducer);
const navigationReducer = persistReducer(config1, NavigationReducer);
const oneSignalReducer = persistReducer(config2, OneSignalReducer);

// combineReducer applied on persisted(counterReducer) and NavigationReducer
const rootReducer = combineReducers({
  userReducer,
  navigationReducer,
  oneSignalReducer
});

const NavMiddleware = createReactNavigationReduxMiddleware(
  "root",
  state => state.nav,
)
function configureStore() {
  let middleware = [thunk]
  let store = createStore(rootReducer, applyMiddleware(...middleware));
  let persistor = persistStore(store);
  return { persistor, store };
}

export default configureStore;
