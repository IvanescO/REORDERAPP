const initialState = { pushToken: null, userNotificationsId: null };

export default function userReducer(state = initialState, action) {
  //return state;
  switch (action.type) {
    case "SET_ONE_SIGNAL_DATA": {
      console.log(action.payload);
      const {payload:{pushToken, userNotificationsId}} = action;
      return { ...state, pushToken, userNotificationsId };
    }
    default:
      return state;
  }
}
