import { combineReducers } from 'redux';
import userReducer from './userReducer';
import navigationReducer from './navigationReducer';
const rootReducer = combineReducers({ userReducer, navigationReducer });

export default rootReducer;