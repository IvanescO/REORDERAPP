import { AsyncStorage } from "react-native";


// const initialState = { user: null, ip: "172.16.3.22", port: "5000" };
// const initialState = { user: null, ip: "172.16.3.22", port: "5000" };
//const initialState = { user: null, ip: "195.35.87.72", port: "12501" };
//const initialState = { user: null, ip: "172.16.3.248", port: "5578" };
//172.16.3.101:20007
// const initialState = { isLoggedIn: false, user:null, ip: "172.16.3.101", port: "20007" };
const initialState = {
  isLoggedIn: false,
  user: null,
  selectedOrder: null,
  loginError: "",
  ordersLoading: false,
  total_debt: 0
};

export default function userReducer(state = initialState, action){
    //return state;
    switch (action.type) {
      case "LOGIN_ERROR":{
        return { ...state, loginError: "Der Benutzername oder das Passwort ist ungültig" };
      }
      case "UPD_USER": {
        return { ...state, user: action.payload };
      }
      case "LOGIN": {
        return { ...state, isLoggedIn: true };
      }
      case "LOGIN": {
        return { ...state, user: action.payload.user, isLoggedIn: true, loginError:' '};
      }
      case "LOGOUT": {
        return { ...state, isLoggedIn: false, user: null, loginError: action.payload.error || "" };
      }
      case "SET_USER": {
        return { ...state, isLoggedIn: true, user: action.payload, loginError: " " };
      }
      case "SET_ORDERS": {
        return { ...state, orders: action.payload.orders, ordersLoading: action.payload.ordersLoading, total_debt: action.payload.total_debt };
      }
      case "SELECT_ORDER": {
        return {...state, selectedOrder: action.payload}
      }
      case "ORDERS_LOADING": {
        return { ...state, ordersLoading: action.payload.ordersLoading };
      }
      default:
        return state;
    }
}

  