import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Button,
  TextInput,
  Keyboard
} from "react-native";
import { connect } from "react-redux";
import { updSettings } from "../actions/userActions";
import { NavigationActions } from "react-navigation";
import { changeRoute } from "../actions/routerAction";

class SettingsScreen extends React.Component {
  static navigationOptions = {
    title: "Settings",
    header: null
  };
  state = { ip: "", port: "" };
  Save = () => {
    let { ip, port } = this.state;
    // const { dispatch } = this.props.navigation;
    this.props.updSettings({ ip, port });
    Keyboard.dismiss();
    const navigateToOrder = NavigationActions.navigate({
      routeName: "Order"
    });
    this.props.changeRoute(navigateToOrder);
  };
  ipHandle = ip => {
    this.setState({ ip });
  };
  portdHandle = port => {
    this.setState({ port });
  };
  componentWillMount() {
    const { user: { port, ip } } = this.props;
    this.setState({ port, ip });
  }
  render() {
    const { navigate } = this.props.navigation;
    const { container, input, inputForm, saveButton } = style;
    let { ip, port } = this.state;
    return (
      <View style={container}>
        <View style={inputForm}>
          <TextInput
            value={ip}
            style={input}
            placeholder="ip"
            underlineColorAndroid="transparent"
            keyboardType="email-address"
            onChangeText={this.ipHandle}
          />
          <TextInput
            value={port}
            style={input}
            placeholder="port"
            underlineColorAndroid="transparent"
            onChangeText={this.portdHandle}
          />
          <Button
            onPress={this.Save}
            color="#29ab9e"
            title={"Save configuration"}
          />
        </View>
      </View>
    );
  }
}
const style = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: "#3487db"
  },
  input: {
    height: 40,
    backgroundColor: "white",
    marginBottom: 10
  },
  inputForm: {
    padding: 20
  }
});
function mapStateToProps(state){
  return {
    user: state.userReducer.user
  }
}
function mapDispatchToProps(dispatch){
  return {
    saveConfig: config => {
      dispatch(updSettings(config))
    }
  }
}
export default connect(mapStateToProps, { updSettings, changeRoute })(
  SettingsScreen
);
