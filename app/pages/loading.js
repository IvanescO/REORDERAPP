import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ActivityIndicator, StyleSheet, View } from 'react-native';
import StyleConst from '../styleConst';
function mapStateToProps(state) {
    return {

    };
}

class LoadingScreen extends Component {
    render() {
        return <View style={style.container}>
            <ActivityIndicator size="large" color={StyleConst.buttonColor} />
          </View>;
    }
}
const style = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: StyleConst.backgroundColor,
    alignItems: "center"
  },
});
export default connect(
    mapStateToProps,
)(LoadingScreen);
