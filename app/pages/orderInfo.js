import React, { Component } from 'react';
import { connect } from 'react-redux';
import ToolBar from "../components/toolBar";
import { View, Text, StyleSheet, ScrollView, ActivityIndicator, Alert } from 'react-native';
import { Card, ListItem, Button, List, Overlay } from "react-native-elements";
import { PricingCard } from "react-native-elements";
import StyleConst from '../styleConst';
import terminalUtils from '../api/terminalUtils';
import { updOrder, addTransaction, selectOrder,Logout } from "../actions/userActions";

// var ProgressBar = require('react-native-progress-bar');
var net = require("react-native-tcp");
import _ from 'lodash';
import orderApi from '../api/orderApi';
import ProgressBarClassic from 'react-native-progress-bar-classic';
 const AUTHORISATION = "0601";
 const SUMM_SUCCESS_CODE = "800000";
 const AUTH_FIELD_AMOUNT = "04";
 const TIME_OUT_ERROR_CODE = "061e";
 const PIN_INCORECT_CODE = "1712";
 const STATUS_INFORMATION = "040f";
function mapStateToProps(state) {
    return { selectedOrder: state.userReducer.selectedOrder, userData: state.userReducer };
}
var Buffer = require("buffer/").Buffer;
import * as Progress from 'react-native-progress';

class orderInfoPage extends Component {
  state = {
    billing: false,
    amount: 0,
    transactionCountLeft: 0,
    transactionCountTotal: 0,
    testPayment: false,
    max_transaction: 1000,
    is_test_payment: 0,
    logs:""
  };
  static navigationOptions = {
    header: null
  };
  componentWillMount() {
    const id = this.props.navigation.state.params.order.id;
    if(!this.props.userData) return;
    const {
      userData: { user: { is_test_payment, max_transaction } }
    } = this.props;
    this.setState({ max_transaction: max_transaction || 1000, is_test_payment });
    orderApi
      .getOrder(id)
      .then(response => {
        // console.log(response);
        // if (!response.data.success && response.data.error == "USER_NOT_FOUND") {
        //   return this.props.Logout(
        //     "Der Benutzername oder das Passwort ist ungültig"
        //   );
        // }
        this.props.selectOrder(response.data);
      })
      .catch(e => {
        if (e.response.data.status === 401) {
          return this.props.Logout("Der Benutzername oder das Passwort ist ungültig");
        }
      });
  }
  terminalError = () => {};
  getProducts = () => {
    const { selectedOrder } = this.props;
    let info = selectedOrder.products.map(product => {
      return `${product.title}  ${product.price}€`;
    });
    info.push(`You paid: ${selectedOrder.amount}€`);
    return info;
  };
  componentWillUnmount() {
    if (this.client) {
      this.client.destroy();
    }
  }
  showAlert = err => {
    if (this.client) {
      this.client.destroy();
      // this.client.close();
    }
    clearTimeout(this.state.timer);
    const { amount, testPayment, logs } = this.state;
    const { selectedOrder } = this.props;
    this.props.addTransaction(selectedOrder.id, amount, 0, logs);
    Alert.alert(
      "Ein Fehler ist während der Bezalung aufgetaucht",
      err,
      [
        {
          text: "Wiederholen",
          onPress: this.startBill.bind(this, testPayment)
        },
        { text: "Abbrechen", style: "cancel" }
      ],
      { cancelable: false }
    );
  };
  showSuccess = async (mes) => {
    if (this.client) {
      this.client.destroy();
      // this.client.close();
    }
    clearTimeout(this.state.timer);
    const { amount, logs } = this.state;
    let { selectedOrder } = this.props;
    this.props.addTransaction(selectedOrder.id, amount, 1, logs);
    
    Alert.alert(
      "Zahlung war erfolgreich",
      mes,
      [{ text: "Ok", style: "cancel" }],
      {
        cancelable: false
      }
    );
  };
  setInPayment = () => {
    this.showSuccess();
    const { selectedOrder } = this.props;
    this.props.updOrder(selectedOrder.id, "in_payment");
  };
  updateCountOfTransaction = ()=> {

  }
  startBill = test => {
    const { selectedOrder } = this.props;
    
    this.setState({ testPayment: test});
    this.resetLogs();
    
    const { userData: { user: { is_test_payment, max_transaction } } } = this.props;
    let step = max_transaction;
    const dif = Number((selectedOrder.cost - selectedOrder.amount).toFixed(2));
    let amount = dif > step ? step : dif; 
    if (test) {
      // amount = 0.01;
      amount = 0.01;
      step = 0.01;
    }
    let transactionCountTotal = Math.ceil(Number(selectedOrder.cost).toFixed(2) / step);
    let transactionCountLeft = Math.ceil(Number(selectedOrder.amount).toFixed(2) / step);
    // if (amount == 0.01) {
    //   transactionCountTotal = Number(selectedOrder.cost).toFixed(2) / step;
    //   transactionCountLeft = Number(selectedOrder.amount).toFixed(2) / step;
    // }
    // transactionCountTotal = Number(transactionCountTotal).toFixed(2);
    // transactionCountLeft = Number(transactionCountLeft).toFixed(2);
    console.log(transactionCountTotal);
    this.setState(
      { amount, transactionCountTotal, transactionCountLeft },
      () => {
        this.billProcess();
      }
    );
  };
  billProcess() {
    const { userData: { user: { terminal_ip, terminal_port } } } = this.props;
    let { amount, logs } = this.state;
    this.setState({ billing: true, status: "" });
    /** test Successful mes
     * // this.showSuccess("Erfolgreiche Bezahlung!");
      // return;
     */
    let timer = setTimeout(() => {
      this.setState({
        billing: false,
        status: "Zahlungsfrist abgelaufen"
      });
      //logs += `ERROR: TIMEOUT EXECUTED,`;
      this.setLogs(`ERROR: TIMEOUT EXECUTED,`);
      console.log("TIMEOUT EXECUTED");
      this.showAlert("Zahlungsfrist abgelaufen");
    }, 120000);
    this.setState({ timer });
    this.client = net.createConnection(
      { port: terminal_port, host: terminal_ip },
      () => {
        console.log("=========AMOUNT===========");
          console.log(amount);
        console.log("==========================");
        //logs += `AMOUNT: ${amount},`;
        this.setLogs(`AMOUNT: ${amount},`);
        amount = Number((amount * 100).toFixed(2));
        //logs += `AMOUNT * 100: ${amount},`;
        this.setLogs(`AMOUNT * 100: ${amount},`);
        amount = amount
          .toString()
          .split(".")
          .join("");
        amount = _.padStart(amount, 12, "0");
        let data = (AUTH_FIELD_AMOUNT + amount).toUpperCase();

        let len = Buffer.from(data, "hex").length.toString();
        len = terminalUtils.base_convert(len, 10, 16);
        len = _.padStart(len, 2, "0");

        let request = AUTHORISATION + len + data;
        console.log("=========FINALREQUEST===========");
        console.log(request)
        //logs += `REQUEST TO THE TERMINAl : ${request},`;
        this.setLogs(`REQUEST TO THE TERMINAl : ${request},`);
        console.log("==========================");
        // let requestInBin = Buffer.from(request, "hex");
        //this.setState({logs});
        let requestInBin = Buffer.from(request, "hex"); // terminalUtils.hex2bin(request, "hex");
        this.client.write(requestInBin);
      }
    );
    this.client.on("data", this.getResponseFromTerminal);
    this.client.on("error", err => {
      console.log('here')
      let { amount, logs } = this.state;
     // logs +="connection is failed";
      this.setLogs("connection is failed");
      //this.setState({logs: logs+"connection is failed"})
      // this.setState({
      //   billing: false,
      //   terminalStatus:
      //     "POS-terminal not found, payment will not be possible, contact technical support REORDER."
      // });
      // clearTimeout(this.state.timer);
      // this.showAlert(
      //   "POS-terminal not found, payment will not be possible, contact technical support REORDER."
      // );
    });
    this.client.on("end", () => {
      this.setState({ billing: false, status: "" });
    });
  }
  getResponseFromTerminal = response => {
    let { amount, logs } = this.state;
    //response = Buffer.from(response, "binary").toString();
    response = terminalUtils.bin2hex(response.toString()).slice(0, 8);

    console.log("=========RESPONSE_FROM_TERMINAL===========");
    console.log(response);
    //logs+=`RESPONSE FROM TERMINAL: ${response},`;
    //this.setState({logs});
    this.setLogs(`RESPONSE FROM TERMINAL: ${response},`);
    console.log("==========================");
    if (response.includes(SUMM_SUCCESS_CODE)) {
      this.setState({ text: "SUMM_SUCCESS_CODE" });

      console.log("=========SUMM_SUCCESS_CODE===========");
       //logs += `SUMM_SUCCESS_CODE,`;
       //this.setState({ logs });
       this.setLogs(`SUMM_SUCCESS_CODE,`);
      console.log("==========================");
    }
    if (response.includes(TIME_OUT_ERROR_CODE)) {
      this.setState({ billing: false, status: "Zahlungsfrist abgelaufen" });
      clearTimeout(this.state.timer);
      console.log("=========TIME_OUT_ERROR_CODE===========");
      console.log(TIME_OUT_ERROR_CODE);
      //logs += `TIME_OUT_ERROR_CODE,`;
      console.log("==========================");
      //this.setState({ logs });
       this.setLogs(`TIME_OUT_ERROR_CODE,`);
      this.showAlert("Zahlungsfrist abgelaufen");
    }
    if (response.includes(PIN_INCORECT_CODE)) {
      this.setState({ billing: false, status: "PIN ERROR" });
      clearTimeout(this.state.timer);
      console.log("=========PIN_INCORECT_CODE===========");
      //logs += `PIN_INCORECT_CODE,`;
      //this.setState({ logs });
       this.setLogs(`PIN_INCORECT_CODE,`);
      console.log("==========================");
      this.showAlert("PIN ist ungültig");
    }
    if (response.includes(STATUS_INFORMATION)) {
      let req = SUMM_SUCCESS_CODE;
      let binReq = Buffer.from(req, "hex");
      console.log("=========STATUS_INFORMATION===========");
      //logs += `STATUS_INFORMATION:${binReq},`;
      //this.setState({ logs });
      this.setLogs(`STATUS_INFORMATION:${binReq},`);
      console.log(logs);
      console.log("==========================");
      this.client.write(binReq);
      this.setState({ text: "STATUS_INFORMATION" });
    }
    if (response.includes("060f")) {
      console.log("=========060f===========");
      console.log("060f");
      console.log("==========================");
      let req = SUMM_SUCCESS_CODE;
      let binReq = Buffer.from(req, "hex");
      //logs += `Final Request1:${binReq},`;
      this.setLogs(`Final Request1:${binReq},`);
      //this.setState({ logs });
      this.client.write(binReq, () => {
        req = "060200";
        binReq = Buffer.from(req, "hex");
        //logs += `Final Request2:${binReq},`;
        this.setLogs(`Final Request2:${binReq},`);
        this.client.write(binReq);
        this.setLogs(`Final Request1:${binReq},`);
        this.setState({ billing: false, status: "Successful" /*,logs*/ });
        clearTimeout(this.state.timer);
        this.showSuccess("Erfolgreiche Bezahlung!");
      });
    }
    //this.setState({ startBiling: false });
  };
  setLogs = (log) => {
    let { logs } = this.state;
    logs += log;
    this.setState({logs});
  }
  resetLogs = () => {
    this.setState({logs:""});
  }
  selectButton = () => {
    const { selectedOrder } = this.props;
    const { billing } = this.state;
    const buttonPay = {
      onButtonPress: !billing ? this.startBill : null,
      button: { title: !billing ? "Bill" : "processing" }
    };
    if (selectedOrder.status == "in_payment") return buttonPay;
  };

  letCountOfTransaction = () => {
    let { amount, logs } = this.state;
    if(!amount) return { transactionCountTotal: null, transactionCountLeft:null };
    if (!this.props.userData.user) return { transactionCountTotal: null, transactionCountLeft: null };
    const { userData: { user: { is_test_payment, max_transaction } } } = this.props;
    let { selectedOrder } = this.props;
    let step = amount == 0.01 ? 0.01: max_transaction;
    const dif = Number((selectedOrder.cost - selectedOrder.amount).toFixed(2));
   
     console.log("*********");
     console.log(selectedOrder.amount, selectedOrder.cost);
     console.log("__*********____");
    // if (amount == 0.01) {
    //     step = 0.01;
    // }
    selectedOrder.cost = Number(selectedOrder.cost.toFixed(2));
    selectedOrder.amount = Number(selectedOrder.amount.toFixed(2));
     let transactionCountTotal = Math.ceil(Number((selectedOrder.cost / step).toFixed(2)));
     let transactionCountLeft = Math.ceil(Number((selectedOrder.amount / step).toFixed(2)));
    return { transactionCountTotal, transactionCountLeft };
  }
  render() {
    const { selectedOrder } = this.props;
    const { container } = styles;
    const { billing, is_test_payment, amount } = this.state;
    let { transactionCountLeft, transactionCountTotal } = this.letCountOfTransaction(amount);
    // console.log(bad);
    const result = transactionCountLeft || transactionCountTotal;
    return <View style={container}>
        <ToolBar showList title="Bezahlung" billing={billing} />
        {selectedOrder && <ScrollView style={container}>
            <Card title={`Bestellung ${selectedOrder.id}`}>
              <Text style={{ textAlign: "center", fontSize: 19, color: "black" }}>
                {Number(selectedOrder.amount).toFixed(2)} € / {Number(selectedOrder.cost).toFixed(2)} €
              </Text>
              {/* <ProgressBarClassic progress={Number((selectedOrder.amount / selectedOrder.cost * 100).toFixed(2))} /> */}
              <List>
                {selectedOrder.products && selectedOrder.products.map(
                    product => {
                      return (
                        <ListItem
                          key={product.id}
                          title={product.title}
                          hideChevron
                          badge={{
                            value: `${(
                              product.price * product.quantity
                            ).toFixed(2)}€`,
                            textStyle: { color: "red" },
                            containerStyle: { backgroundColor: "white" }
                          }}
                        />
                      );
                    }
                  )}
              </List>
              <Progress.Bar showsText={true} borderRadius={0} progress={Number((selectedOrder.amount / selectedOrder.cost).toFixed(2))} color="red" width={null} height={25} />
              <View>
                {result ? <Text style={{ textAlign: "center", fontSize: 19, marginVertical: 10, color: "black" }}>
                    {transactionCountLeft} transaktionen von {transactionCountTotal}
                  </Text> : null}
              </View>
              {!billing && <Button icon={{ name: "payment" }} backgroundColor={StyleConst.buttonColor} buttonStyle={{ borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0, marginTop: 10 }} onPress={() => this.startBill(false)} title="Bezahlen" />}
              {Boolean(is_test_payment) && !billing && <Button icon={{ name: "payment" }} backgroundColor={StyleConst.buttonColor} buttonStyle={{ borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0, marginTop: 10 }} onPress={() => this.startBill(true)} title="Test-Bezahlen 0.01 €" />}
              {billing && <ActivityIndicator size="large" color="#000" />}
              <Text
                style={{
                  textAlign: "center",
                  fontSize: 13,
                  color: "black",
                  paddingTop: 10
                }}
              >
                Bitte achten Sie darauf die Anwendung während des
                Bezahlvorgangs mit dem Terminal nicht zu beenden.
              </Text>
            </Card>
          </ScrollView>}
      </View>;
    <Overlay isVisible>
      <Text>{this.state.status} dsadsadas dsadsadasdasdas</Text>
      <Button
        icon={{ name: "payment" }}
        backgroundColor="#03A9F4"
        fontFamily="Lato"
        buttonStyle={{
          borderRadius: 0,
          marginLeft: 0,
          marginRight: 0,
          marginBottom: 0,
          marginTop: 10
        }}
        onPress={this.setState({ showOverlay: false })}
        title="PAY"
      />}
    </Overlay>;
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: StyleConst.backgroundColor
  }
});

export default connect(mapStateToProps, {
  updOrder,
  addTransaction,
  selectOrder,
  Logout
})(orderInfoPage);