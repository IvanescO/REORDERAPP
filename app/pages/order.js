
import React from "react";
import { StyleSheet, Text, View, Button, TouchableHighlight, ActivityIndicator, AsyncStorage, ScrollView, List, FlatList, ListItem, RefreshControl } from "react-native";
var net = require("react-native-tcp");
import _ from 'lodash';
import { connect } from "react-redux";
import { NavigationActions } from "react-navigation";
import { changeRoute } from "../actions/routerAction";
import { Logout, getOrders, updOrder, selectOrder } from '../actions/userActions';
import StyleConst from "../styleConst";
import Icon from "react-native-vector-icons/FontAwesome";
import terminalUtils from '../api/terminalUtils';
import OrderService from '../api/orderApi';
import ToolBar from '../components/toolBar';


 const AUTHORISATION = "0601";
 const SUMM_SUCCESS_CODE = "800000";
 const AUTH_FIELD_AMOUNT = "04";
 const TIME_OUT_ERROR_CODE = "061e";
 const PIN_INCORECT_CODE = "1712";
 const STATUS_INFORMATION = "040f";
class OrderScreen extends React.Component {
  static navigationOptions = {
    header: null
  };
  state = {
    text: "",
    startPayment: "",
    conectedToTerminal: false,
    startBiling: false,
    startConect: false,
    terminalStatus: "",
    order: null
  };
  conectToTerminal = () => {
    this.setState({ startConect: true, terminalStatus: "" });
    const { userData: { user: { terminal_ip, terminal_port } } } = this.props;
    this.client = net.createConnection(
      { port: terminal_port, host: terminal_ip },
      data => {
        console.log("connected to server!");
        this.setState({
          conectedToTerminal: true,
          terminalStatus: "Connection successful",
          startConect: false
        });
        this.Bill();
        //this.client.write("dsadasdsa!\r\n");
      }
    );
    this.client.on("data", this.getResponseFromTerminal);
    this.client.on("error", err => {
      console.log(err);
      this.setState({
        terminalStatus:
          "POS-terminal not found, payment will not be possible, contact technical support REORDER.",
        startConect: false
      });
    });
    this.client.on("end", () => {
      console.log("disconnected from server");
    });
  };
  disconectToTerminal = () => {
    this.client.end();
    this.setState({
      conectedToTerminal: false,
      text: "",
      terminalStatus: "",
      startBiling: false
    });
  };

  componentWillUnmount() {
    if (this.client) this.client.destroy();
  }
  async componentWillMount() {
    this.props.getOrders();
  }
  getResponseFromTerminal = response => {
    response = terminalUtils.bin2hex(response.toString()).slice(0, 4);

    console.log("-------------------");
    console.log(response);
    console.log("-------------------");
    if (response == SUMM_SUCCESS_CODE) {
      this.setState({ text: "SUMM_SUCCESS_CODE" });
    }
    if (response == TIME_OUT_ERROR_CODE) {
      this.setState({ text: "TIME_OUT_ERROR_CODE" });
    }
    if (response == PIN_INCORECT_CODE) {
      this.setState({ text: "TIME_OUT_ERROR_CODE" });
    }
    if (response == STATUS_INFORMATION) {
      let req = SUMM_SUCCESS_CODE;
      let binReq = terminalUtils.hex2bin(req);
      this.client.write(binReq);
      this.setState({ text: "STATUS_INFORMATION" });
    }
    if (response == "060f" || response == "0000") {
      let req = SUMM_SUCCESS_CODE;
      let binReq = terminalUtils.hex2bin(req);
      this.client.write(binReq);

      req = "060200";
      binReq = terminalUtils.hex2bin(req);
      this.client.write(binReq);

      this.setState({ text: this.state.text.concat("SUCCESS BILLED ") });
    }
    this.setState({ startBiling: false });
  };
  Bill = () => {
    const { startBiling } = this.state;
    this.setState({ startBiling: true });
    let amount = 0.01;
    let sec = 60;
    if (startBiling) {
      let timer = setInterval(() => {
        sec--;
        if (sec == 0) {
          clearInterval(timer);
          this.setState({ startBiling: false });
        }
        this.setState({ startPayment: sec });
      }, 1000);
    }

    amount = amount * 100;
    amount = amount.toString();
    amount = _.padStart(amount, 12, "0");

    let data = (AUTH_FIELD_AMOUNT + amount).toUpperCase();

    let len = terminalUtils.hex2bin(data).length.toString();
    len = terminalUtils.base_convert(len, 10, 16);
    len = _.padStart(len, 2, "0");

    let request = AUTHORISATION + len + data;
    let requestInBin = terminalUtils.hex2bin(request);
    this.client.write(requestInBin);
  };
  goToSettings = () => {
    const navigateToSettings = NavigationActions.navigate({
      routeName: "Settings"
    });
    this.props.changeRoute(navigateToSettings);
  };
  Logout = () => {
    this.props.Logout();
  };
  updOrderStatus(id, status) {
    this.props.updOrder(id, status);
  }
  selectOrder(order) {
    console.log(order.id);
    const navigateToOrderInfo = NavigationActions.navigate({
      routeName: "orderInfo",
      params: { order }
    });
    this.props.selectOrder(order);
    console.log(order);
    this.props.changeRoute(navigateToOrderInfo);
  }
  getOrders() {
    let { userData: { user, orders, total_debt } } = this.props;

    orders = orders
      .map((order, index) => {
        return <View key={order.id} style={{ padding: 20, borderColor: "#b1b1b1f0", borderWidth: 0.5, flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
            <View style={{ flexDirection: "column" }}>
              <Text style={{ color: "#808080", alignItems: "center" }}>
                Bestellung {order.id}
              </Text>
              <Text style={{ color: "#808080", alignItems: "center", fontSize: 13 }}>
                {Number(order.amount).toFixed(2)} € / {Number(order.cost).toFixed(2)} €
              </Text>
            </View>
            <View style={{ flexDirection: "row" }}>
              <TouchableHighlight style={{ marginRight: 30 }} onPress={() => this.selectOrder(order)}>
                <Icon name="shopping-cart" size={25} color="#808080" />
              </TouchableHighlight>
              <TouchableHighlight onPress={() => this.updOrderStatus(order.id, "cancel")}>
                {/* <Text style={{ color: "white" }}>Decline</Text>
               */}
                <Icon name="times-circle" size={25} color={StyleConst.buttonColor} />
              </TouchableHighlight>
            </View>
          </View>;
      })
      if(total_debt){
        orders.unshift(<View key={"debt"} style={{ padding: 20, borderColor: "#b1b1b1f0", borderWidth: 0.5, flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
            <Text>Du hast Schulden: {total_debt}€ </Text>
          </View>);
      }
      return orders;
  }
  Refesh = () =>{
    this.props.getOrders();
  };
  render() {
    const {
      text,
      startPayment,
      conectedToTerminal,
      startBiling,
      terminalStatus,
      startConect,
      order
    } = this.state;
    const { container, content, statusText, buttonBlock } = styles;
    const { userData: { user, orders }, ordersLoading } = this.props;
    return <View style={container}>
        <ToolBar title={"Bestellungen"} />
        {!ordersLoading && <ScrollView style={container}>
            <View style={content}>
              <Text style={{ textAlign: "right", paddingRight:20, marginBottom: 20, marginTop:20 }} onPress={this.Refesh}>
                <Icon name="retweet" size={23} color="#808080" />
              </Text>
              <ScrollView>
                {orders && this.getOrders()}
                {orders && !orders.length && <Text
                      style={{
                        textAlign: "center",
                        color: "#808080",
                        fontSize: 18
                      }}
                    >
                      Sie haben keine Bestellungen
                    </Text>}
              </ScrollView>
            </View>
          </ScrollView>}
        {ordersLoading && <View style={{ alignContent: "center", justifyContent: "center", marginTop: 40 }}>
            <ActivityIndicator size="large" color={StyleConst.buttonColor} />
          </View>}
      </View>;
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: StyleConst.backgroundColor
  },
  buttonBlock: {
    marginBottom: 20
  },
  orderButton: {
    color: "#000"
  },
  statusText: {
    color: "#000",
    textAlign: "center"
  },
  content: {
    //flex: 1,
    //alignItems: 'center',
    padding: 0
  }
});
function mapStateToProps(state) {
  return {
    userData: state.userReducer,
    ordersLoading: state.userReducer.ordersLoading
  };
}
export default connect(mapStateToProps, {
  changeRoute,
  Logout,
  getOrders,
  updOrder,
  selectOrder
})(OrderScreen);