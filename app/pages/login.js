import React from "react";
import { StyleSheet, Text, View, TextInput, Keyboard, Image,TouchableHighlight } from "react-native";
import  {userLogin} from '../actions/userActions';
import { connect } from "react-redux";
import StyleConst from "../styleConst";
import { Button } from "react-native-elements";
import Icon from "react-native-vector-icons/FontAwesome";
class LoginScreen extends React.Component {
  static navigationOptions = {
    header: null
  };
  state = { login: "", password: "", loginError: "" };
  
  Login = () => {
    let { login, password } = this.state;
    const { navigate } = this.props.navigation;
    const { userLogin, oneSignalData: { userNotificationsId } } = this.props;
    Keyboard.dismiss();
    userLogin(login, password, userNotificationsId);
  };
  loginHandle = login => {
    this.setState({ login, loginError: "" });
  };
  passwordHandle = password => {
    this.setState({ password, loginError: "" });
  };
  render() {
    const { navigate } = this.props.navigation;
    const {
      container,
      input,
      inputForm,
      loginButton,
      imgStyle,
      searchIcon
    } = style;
    let { login, password } = this.state;
    return <View style={container}>
        <View style={imgStyle}>
          <Image style={{width: 400,
    height: 200}} source={require("../assets/logo2.png")} />
        </View>
        <View style={{ flexDirection: "column", alignSelf: "stretch", paddingHorizontal: 30 }}>
          <View style={inputForm}>
            <Icon style={searchIcon} name="user" size={20} color="#fff" />
            <TextInput value={login} style={input} placeholder="Benutzername" underlineColorAndroid="transparent" keyboardType="email-address" onChangeText={this.loginHandle} autoCapitalize="none" />
          </View>
          <View style={inputForm}>
            <Icon style={searchIcon} name="lock" size={20} color="#fff" />
            <TextInput value={password} style={input} placeholder="Passwort" underlineColorAndroid="transparent" secureTextEntry={true} onChangeText={this.passwordHandle} autoCapitalize="none" />
            
          </View>
          {/* <View style={inputForm}>
          <TouchableHighlight style={{flex: 1}}>
            <Text> Login </Text>
          <TouchableHighlight/>
          <View> */}
          <Text style={{ color: "red", textAlign: "center" }}>
              {this.props.loginError}
            </Text>
          <View style={inputForm}>
            <TouchableHighlight style={{flex:1, height: 55, backgroundColor: StyleConst.buttonColor,alignItems:'center',justifyContent:"center" }} onPress={this.Login}> 
              <Text style={{color: '#fff', textAlign:'center', fontSize: 18}}> Anmelden </Text>
           </TouchableHighlight>
          </View>
        </View>
      </View>;
  }
}

const style = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: StyleConst.backgroundColor,
    alignItems: "center"
  },
  imgStyle: {
    alignItems: "center",
    paddingBottom: 20
  },
  inputForm: {
    marginBottom: 10,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "stretch",
    backgroundColor: "#fff"
  },
  searchIcon: {
    padding: 14,
    paddingTop: 16,
    paddingBottom: 16,
    backgroundColor: StyleConst.buttonColor
  },
  input: {
    flex: 1,
    paddingTop: 10,
    paddingRight: 30,
    paddingBottom: 10,
    borderWidth: 2, // size/width of the border
    borderColor: "lightgrey", // color of the border
    // paddingLeft: 10,
    paddingLeft: 15,
    backgroundColor: "white",
    color: "#424242"
  }
});
export default connect((state)=>{
  return { oneSignalData: state.oneSignalReducer, loginError: state.userReducer.loginError };
},{ userLogin })(LoginScreen);
