import React from "react";
import NavigationApp from './app/routes'
import { Provider } from 'react-redux';
import { createStore, applyMiddleware} from "redux";
// import RootReducer from './app/reducers';
import { PersistGate } from "redux-persist/es/integration/react";
import { View, Text } from "react-native";
import configureStore from "./app/store";
import LoadingScreen from "./app/pages/loading";
const { store, persistor } = configureStore();

//import thunk from "redux-thunk";

//const store = createStore(RootReducer, applyMiddleware(thunk));

export default class App extends React.Component {
  render() {
    return <Provider store={store}>
        <PersistGate loading={<LoadingScreen/>} persistor={persistor}>
          <NavigationApp />
        </PersistGate>
      </Provider>;
  }
}
